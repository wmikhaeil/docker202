FROM python
COPY /helloworld.py .
WORKDIR /requirements.txt .
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["helloworld.py"]